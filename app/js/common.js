$(document).ready(function(){
  $(".hamburger").click(function(){

    $(this).toggleClass("is-active");

    $(".mob-menu").toggle(300);

  });  //hamb-menu


  $('.bxslider').bxSlider({
  	mode: 'fade',
  	auto: true,
  	speed: 700,
  	pager: false
  }); //slider


  $(".contact").click(function(){
  	$(".pop-up-cont").fadeIn(1800);
  });
  $(".close").click(function(){
  	$(".pop-up-cont").fadeOut(1800);
  }); // pop-up



  //form validation
  $('input#name, input#mail').unbind().blur( function(){
             
            
        var id = $(this).attr('id');
        var val = $(this).val();

        switch(id){
                 case 'name':
                    var rv_name = /^[a-zA-Zа-яА-Я]+$/;


                    if(val.length > 2 && val != '' && rv_name.test(val))
                    {
                       $(this).addClass('not_error');
                       $(this).next('.error-box').text('Accept')
                                                 .css('color','green')
                                                 .animate({'paddingLeft':'10px'},400)
                                                 .animate({'paddingLeft':'5px'},400)
                                                 .animate({'paddingLeft':'0px'},400);
                    }
                    else
                    {
                       $(this).removeClass('not_error').addClass('error');
                       $(this).next('.error-box').html('&bull; field "Name" should be filled<br> &bull; length more than 2 symbols<br> &bull; only russian and latin symols')
                                                  .css('color','red')
                                                  .animate({'paddingLeft':'10px'},400)
                                                  .animate({'paddingLeft':'5px'},400)
                                                  .animate({'paddingLeft':'0px'},400);
                    }
                break;
                
               case 'mail':
                   var rv_email = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
                   if(val != '' && rv_email.test(val))
                   {
                      $(this).addClass('not_error');
                      $(this).next('.error-box').text('Accept')
                                                .css('color','green')
                                                .animate({'paddingLeft':'10px'},400)
                                                  .animate({'paddingLeft':'5px'},400)
                                                  .animate({'paddingLeft':'0px'},400);
                   }
                   else
                   {
                      $(this).removeClass('not_error').addClass('error');
                      $(this).next('.error-box').html('&bull; field "Email" should be filled<br> &bull; field should have correct email address<br> (for example: example123@mail.ru)')
                                                 .css('color','red')
                                                 .animate({'paddingLeft':'10px'},400)
                                                  .animate({'paddingLeft':'5px'},400)
                                                  .animate({'paddingLeft':'0px'},400);
                   }
               break;

           } 

         }); 

	//end form validation
}); 
